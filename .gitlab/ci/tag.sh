#!/bin/bash
set +e
ACT=$(git branch --show-current )
echo $ACT

ACTBRANCH=$(git branch --show-current | cut -d '/' -f 1)

if [ "$ACTBRANCH" != "release" ] ; then
    echo "The branch must be release" 
    exit 1
elif [ "$PROJ_VER" = "$CI_COMMIT_TAG" ] ; then 
    echo "Branch is correct and Version/Tag too" 
    exit 0
fi
echo  " ERROR Composer version $PROJ_VER  and TAG  $ACTBRANCH are not equal" 
exit 1

    